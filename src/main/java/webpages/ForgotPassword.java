package webpages;

import basePage.Page;

public class ForgotPassword extends Page {
	
	public void forgotPasswordNav(){
		
		 click("Registration_XPATH");
	        
	     click("LoginSelect_XPATH");
	     
	     click("ForgotPassword_XPATH");
	     
	     
	}
	
	public void forgotPasswordPopUp(String Email) {
		
		type("EmailPasswordField_XPATH", Email);
		
		click("SendPassword_XPATH");
		
		submissionMessage("SubmissionMsg_XPATH");
		
		click("CloseForgotPass_XPATH");
	}

}
