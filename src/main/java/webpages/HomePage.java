package webpages;

import basePage.Page;

public class HomePage extends Page{
	
	
	public void doRegistration(String Email) {
		
		click("Registration_XPATH");
    
    	click("RegistrationSelect_XPATH");
    	
    	type("Email_XPATH", Email);
  	
    	//type("FirstName_XPATH", FirstName);
    	
    	//type("LastName_XPATH", LastName);
    	
    	//type("ConfirmEmail_XPATH", ConfirmEmail);
   	
    	//type("Password_XPATH", Password);
    	
    	//type("ConfirmPassword_XPATH", ConfirmPassword);
    	
    	//click("NewsletterCheckbox_XPATH");
    	
    	click("TermsCheckbox_XPATH");
    	
    	click("RegistrationSubmit_XPATH");
    	
    	//submissionMessage("RegistrationSuccessMsg_XPATH");
    	
    	//click("RegistrationMsgOk_XPATH");
    	
    	//click("CloseReg_XPATH");
    
    }
	    	

    
    
    public PBClassicDetailPage doLogin(String Email, String Password) {
    	
    	
        click("Registration_XPATH");
        
        click("LoginSelect_XPATH");
    	
    	type("LoginEmail_XPATH", Email);
    	
    	type("LoginPassword_XPATH", Password);
    	
    	click("LoginButton_XPATH");
    	
    	return new PBClassicDetailPage();
    	
    	
    }
    
    public void doForgotPassword() {
    	
    	click("");
    	
    	type("", "");
    	
    	click("");
    	
    	click("");
    	
    	
    }


}