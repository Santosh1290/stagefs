package webpages;

import basePage.Page;

public class PBClassicDetailPage extends Page{
	
	
	public PBClassicHThemesPage NavToClassicHardThemeList() {
		
		click("PB_NavToTheme_XPATH");
		
		return new PBClassicHThemesPage();
		
	}
	

}
