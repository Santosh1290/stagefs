package webpages;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;

import editorPages.UploadPopup;

public class PBClassicHThemesPage extends PBClassicDetailPage{
	
	public void SelectTheme() {
		
		/* Code For Scrolling Using Actions class and send key method.
		 * Actions a = new Actions(driver); a.sendKeys(Keys.PAGE_DOWN).perform();
		 */
	
		
		/*
		 * try { Thread.sleep(3000); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 */
		
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
		
		click_wait("PB_TripToSeaTheme1_XPATH");
		
		isElementPresent("PB_EditorWithtOutTheme_XPATH");

		
		
	}
	
	public UploadPopup NavtoClassic2128Editor() {
		
		click_wait("PB_H_NavToEditor_XPATH");
		
		Set<String> winids = driver.getWindowHandles();
		Iterator<String> iterator = winids.iterator();
		
		System.out.println(iterator.next()); //1st
		String second_window = iterator.next(); //2nd
		System.out.println(second_window);
		
		driver.switchTo().window(second_window);
		
		return new UploadPopup();
	}

}
