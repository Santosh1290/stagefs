package webpages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import basePage.Page;

public class MyCartPage extends Page {
	
	
	public void selectPaperType() {
		
		//WebElement e = driver.findElement(By.xpath("//div[@class=\"loader\"]"));
		
		//wait.until(ExpectedConditions.invisibilityOf(e));
		
		/*
		 * try { Thread.sleep(4000); } catch (InterruptedException e1) { // TODO
		 * Auto-generated catch block e1.printStackTrace(); }
		 */
		
		//selectDropDownValueWait("PaperTypeOptions_XPATH", " Seiden-Matt");
		
		selectDropDownValue("PaperTypeOptions_XPATH", " Seiden-Matt");
		
  	          //WebElement web = driver.findElement(By.xpath(OR.getProperty("PaperTypeOptions_XPATH")));
			  
			  //Select sel = new Select(web);
			  
			  //sel.selectByValue("1: Object");
		
	
			  
	}
	
	
	public void shippingDetails(String FirstName, String LastName, String CompanyName, String StreetName, String HouseNumber, String ZipCode, String City, String PhoneNumber, String Email) {
		
		type("ShippingFirstName_XPATH", FirstName);
		type("ShippingLastName_XPATH", LastName);
		type("ShippingCompanyName_XPATH", CompanyName);
		type("ShippingStreetName_XPATH", StreetName);
		type("ShippingHouserNum_XPATH", HouseNumber);
		type("ShippingZipCode_XPATH", ZipCode);
		type("ShippingCity_XPATH", City);
		type("ShippingPhoneNum_XPATH", "12345678");
		type("ShippingEmail_XPATH", Email);
		type("ShippingPhoneNum_XPATH", PhoneNumber);
		
		click("SameAsShippingCheck_XPATH");
		
		
	}
	
	public void billingDetails() {
		
		
	}
	
	public CheckoutPage navToCheckout() {
		
		 click("NavToCheckoutBtn_XPATH");
		 
		 click("ConfirmationAddress_XPATH");
		
		return new CheckoutPage();
	}

}
