package webpages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import basePage.Page;

public class CheckoutPage extends Page{
	
	public void buyNow() {
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
		
		
		  click_wait("TermsCheckBox_XPATH");
		  
			/*
			 * try { Thread.sleep(1000); } catch (InterruptedException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 */
		  
		  click("OrderBtn_XPATH");
		  
		  //click("InvoiceBtn_XPATH");
		  
		  
		  //click_wait("InvoiceConfirmCheck_XPATH");
		  
		 
		  //click_wait("InvoiceConfirm_XPATH");
		
	}
	
	public void creditCardPayment() {
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,500)");
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		
		click_wait("CreditCardBtn_XPATH");
		
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
        List<WebElement> frames = driver.findElements(By.tagName("iframe"));
		
		System.out.println("Total frames are : "+frames.size());
        
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(OR.getProperty("CardNumIframe1"))));
		
		//driver.switchTo().frame(1);
		
		//driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[title='Fill Quote']")));
		
        System.out.println("in Frame 1");
		
		type("CreditCardNum_XPATH", "4111 1111 1111 1111");
		
//		try {
//			Thread.sleep(2000);
//		} catch (InterruptedException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		
		 //wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(OR.getProperty("CardNumIframe2"))));

		driver.switchTo().defaultContent();
		
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(OR.getProperty("CardNumIframe2"))));
		
		//WebElement frame2 = driver.findElement(By.xpath("//div[@class=\"adyen-checkout__field adyen-checkout__field--50 adyen-checkout__field--expiryDate\"]/label/span/span/iframe[@class=\"js-iframe\"]"));
		
		//driver.switchTo().frame(frame2);
	    
		System.out.println("in Frame 2");
		
		type("CreditCardExp_XPATH", "10/20");
		
		driver.switchTo().defaultContent();
		
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath(OR.getProperty("CardNumIframe3"))));
		
		type("CreditCardCvvTextField_XPATH", "737");
		
		driver.switchTo().defaultContent();
		
		type("CreditCardCustomerName_XPATH", "Sam");
		
		click("CreditCardSaveCheck_XPATH");
		
		click("CreditCardPayBtn_XPATH");
		
		
//		
//		type("CreditCardCvvTextField_XPATH", "737");
//		
//		type("CreditCardCustomerName_XPATH", "Sam");
		
		
	}


}
