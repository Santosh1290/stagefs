package utilities;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import basePage.Page;


public class TestReportEmail extends Page{
	
	
	public static void MailSend(String[] args) throws AddressException, MessagingException {


		MonitoringMail mail = new MonitoringMail();
		mail.sendMail(TestConfig.server, TestConfig.from,TestConfig.to, TestConfig.subject, TestConfig.messageBody, TestConfig.attachmentPath, TestConfig.attachmentName);

	}

}
