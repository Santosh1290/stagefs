package editorPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import basePage.Page;

public class UploadPopup extends Page {
	
	
	public void uploadNewImages() {
		
		//click("UploadNewImages_XPATH");
		
		//driver.findElement(By.xpath("//a[@data-action-type='upload_photo']/div/input")).sendKeys("C:\\Users\\WAY2AUTOMATION\\Desktop\\selenium4.png");
		
		
		
		for(int i = 1; i<=2; i++) {
			
			driver.findElement(By.xpath(OR.getProperty("UploadNewImages_XPATH"))).sendKeys("C:\\Users\\Santosh Myqsoft\\Desktop\\Images\\PF Images\\Beach1_101.jpg");
			
			click("AddImages_XPATH");
		}
		
		click("ProjectImages_XPATH");
		
		click("NavigateToPreparePage_XPATH");
	}
	
	public void uploadFromAlbums() {
		
		/*
		 * Set<String> winids = driver.getWindowHandles(); Iterator<String> iterator =
		 * winids.iterator();
		 * 
		 * System.out.println(iterator.next()); //1st String second_window =
		 * iterator.next(); //2nd System.out.println(second_window);
		 * 
		 * driver.switchTo().window(second_window);
		 */
		
		click_wait("MyAlbums_XPATH");
		
		
	}
	
	public AlbumDetailsPage selectAlbumImages() {
		
		click("AlbumDetails_XPATH");
		
		return new AlbumDetailsPage();
		
		
	}

}
