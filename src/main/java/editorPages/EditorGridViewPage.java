package editorPages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import basePage.Page;
import webpages.MyCartPage;

public class EditorGridViewPage extends Page{
	
	
	public MyCartPage orderBook() {
		
		
		/* CODE FOR CLICKING ELEMENT STAIGHT AWAY WITH HELP OF JAVA SCRIPT
		 * WebElement cartIcon =
		 * driver.findElement(By.xpath("//div[@class=\"global_tools right\"]/ul/li/a"));
		 * 
		 * JavascriptExecutor executor = (JavascriptExecutor)driver;
		 * 
		 * executor.executeScript("arguments[0].click();", cartIcon);
		 */
		
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(OR.getProperty("AILoadingLayer_XPATH"))));
		
        click("CartIcon_XPATH");
        
        click("EditorOrderButton_XPATH");
        
        if(isElementPresent("QualityCheckPop_XPATH")) {
        	
        	driver.findElement(By.xpath(OR.getProperty("QualityCheckPop_XPATH"))).click();
        }
        
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@id=\"toast-container\"]")));
		
//		Set<String> winids = driver.getWindowHandles();
//		Iterator<String> iterator = winids.iterator();
	
//		String firstwindow = iterator.next();
		
//		driver.switchTo().window(firstwindow);
		
		String t = "FotoSystem | beflügelt deine Kreativität";
		
		wait.until(ExpectedConditions.titleIs(t));
		
		String MyCartTitle = driver.getTitle();
		
		log.info("Title For Cart Page Is" + MyCartTitle);
		
		return new MyCartPage();
		
	}
	

}
