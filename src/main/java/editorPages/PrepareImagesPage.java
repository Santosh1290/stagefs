package editorPages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import basePage.Page;

public class PrepareImagesPage extends Page {

	public void selectAllImages() {

		click("SelectAllImages_XPATH");

	}

	public void selectCoverImages() {

		hover("HoverBackCoverImage_XPATH");

		click("ClickBackCoverImage_XPATH");

		hover("HoverFrontCoverImage_XPATH");

		click("ClickFrontCoverImage_XPATH");

		click("DesignNow_XPATH");

	}

	public void designOptions() {

		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 

		click("AutoDesignOption_XPATH");

	}

	public EditorGridViewPage autoDesignPopUp() {
		
		click("AIButton_XPATH");
		
		return new EditorGridViewPage();

	}

}
