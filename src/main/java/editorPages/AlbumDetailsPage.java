package editorPages;

import basePage.Page;

public class AlbumDetailsPage extends Page {
	
	
	public void selectImages() {
		
		click("SelectAlbumImage_XPATH");
		click("SelectAllAlbumImages_XPATH");
	}
	
	public void cancelSelectedImages() {
		
		
	}
	
	public void backToUpload() {
		
		
	}
	
	public void DeleteSelectedImages() {
		
		
	}
	
	public void SelectedImagesCount() {
		
		
	}
	
	public void NavToProjectImagesTab() {
		
		click("NavigateToProjectImagesTab_XPATH");
	}
	
	public PrepareImagesPage NavigateToPrepare() {
		
		click("NavigateToPreparePage_XPATH");
		
		return new PrepareImagesPage();
	}

}
