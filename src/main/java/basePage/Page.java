package basePage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;
import utilities.ExcelReader;
import utilities.MonitoringMail;
import utilities.TestConfig;

public class Page {
	
	
	
	public static WebDriver driver;
	public static Properties OR = new Properties();
	public static Properties Config = new Properties();
	public static FileInputStream fis;
	public static ExcelReader excel = new ExcelReader("./src/main/resources/excel/testdata.xlsx");
	public static MonitoringMail mail = new MonitoringMail();
	public static Logger log = Logger.getLogger(Page.class.getSimpleName());
	public static WebDriverWait wait;
	public static WebElement dropdown;
	public static TopNavigation topNav;
	//public static TestConfig testConfig;
	

	
	
	
	public static boolean isElementPresent(String key) {

		try {
		if (key.endsWith("_XPATH")) {
			driver.findElement(By.xpath(OR.getProperty(key)));
		} else if (key.endsWith("_CSS")) {
			driver.findElement(By.cssSelector(OR.getProperty(key)));
		} else if (key.endsWith("_ID")) {
			driver.findElement(By.id(OR.getProperty(key)));
		}
		log.info("Finding an Element : " + key);
		return true;
		}catch(Throwable t) {
			
			log.info("Error while finding an Element : "+key+" error message is: "+t.getMessage());
			return false;
		}
	}
	
	
	public static boolean submissionMessage(String key) {

		try {
			if (key.endsWith("_XPATH")) {
			String msg	= driver.findElement(By.xpath(OR.getProperty(key))).getText();
				log.info(msg);
			} else if (key.endsWith("_CSS")) {
				String msg = driver.findElement(By.cssSelector(OR.getProperty(key))).getText();
				log.info(msg);
			} else if (key.endsWith("_ID")) {
				String msg = driver.findElement(By.id(OR.getProperty(key))).getText();
				log.info(msg);
			}
		
		return true;
		}catch(Throwable t) {
			
			log.info("Submission isn't successfull with : "+key+" error message is: "+t.getMessage());
			return false;
		}
	}


	public static void click(String key) {

		try {
		if (key.endsWith("_XPATH")) {
			driver.findElement(By.xpath(OR.getProperty(key))).click();
		} else if (key.endsWith("_CSS")) {
			driver.findElement(By.cssSelector(OR.getProperty(key))).click();
		} else if (key.endsWith("_ID")) {
			driver.findElement(By.id(OR.getProperty(key))).click();
		}
		log.info("Clicking on an Element : " + key);
		}catch(Throwable t) {
			
			log.info("Error while clicking on an Element : "+key+" error message is: "+t.getMessage());
			Assert.fail(t.getMessage());
		}
	}

	public static void click_wait(String key) {

		try {
		if (key.endsWith("_XPATH")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(key)))).click();	
		} else if (key.endsWith("_CSS")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(OR.getProperty(key)))).click();
		} else if (key.endsWith("_ID")) {
			wait.until(ExpectedConditions.elementToBeClickable(By.id(OR.getProperty(key)))).click();
		}
		log.info("Clicking on an Element : " + key);
		
		}catch(Throwable t) {
			
			log.info("Error while clicking on an Element : "+key+" error message is: "+t.getMessage());
			Assert.fail(t.getMessage());
		}
	}
	
	
	public static void selectDropDownValue(String key, String value) {

		try {
		if (key.endsWith("_XPATH")) {
			dropdown = driver.findElement(By.xpath(OR.getProperty(key)));
		} else if (key.endsWith("_CSS")) {
			dropdown = driver.findElement(By.cssSelector(OR.getProperty(key)));
		} else if (key.endsWith("_ID")) {
			dropdown = driver.findElement(By.id(OR.getProperty(key)));
		}
		
		Select select = new Select(dropdown);
		select.selectByVisibleText(value);
		log.info("Selecting an Element : " + key+"  selected value as : "+value);
		}catch(Throwable t) {
			
			log.info("Error while selecting on an Element : "+key+" error message is: "+t.getMessage());
			Assert.fail(t.getMessage());
		
		}
	}

	public static void selectDropDownValueWait(String key, String value) {

		try {
		if (key.endsWith("_XPATH")) {
			dropdown = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty(key))));
		} else if (key.endsWith("_CSS")) {
			dropdown = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(OR.getProperty(key))));
		} else if (key.endsWith("_ID")) {
			dropdown = wait.until(ExpectedConditions.elementToBeClickable(By.id(OR.getProperty(key))));
		}
		
		Select select = new Select(dropdown);
		select.selectByVisibleText(value);
		log.info("Selecting an Element : " + key+"  selected value as : "+value);
		}catch(Throwable t) {
			
			log.info("Error while selecting on an Element : "+key+" error message is: "+t.getMessage());
			Assert.fail(t.getMessage());
		
		}
	}
	
	public static void type(String key, String value) {

		try {
			if (key.endsWith("_XPATH")) {
				driver.findElement(By.xpath(OR.getProperty(key))).sendKeys(value);
			} else if (key.endsWith("_CSS")) {
				driver.findElement(By.cssSelector(OR.getProperty(key))).sendKeys(value);
			} else if (key.endsWith("_ID")) {
				driver.findElement(By.id(OR.getProperty(key))).sendKeys(value);
			}
			log.info("Typing in an Element : " + key+"  entered the value as : "+value);
			}catch(Throwable t) {
				
				log.info("Error while typing in an Element : "+key+" error message is: "+t.getMessage());
				Assert.fail(t.getMessage());
			}
	}
	
	
	public static void hover(String key) {

		try {
		if (key.endsWith("_XPATH")) {
			
            WebElement webElement = driver.findElement(By.xpath(OR.getProperty(key)));
        	
        	Actions act = new Actions(driver);
        	
            act.moveToElement(webElement).perform();;
			
		} else if (key.endsWith("_CSS")) {
			
            WebElement webElement = driver.findElement(By.cssSelector(OR.getProperty(key)));
        	
        	Actions act = new Actions(driver);
        	
            act.moveToElement(webElement).perform();;
			
			
		} else if (key.endsWith("_ID")) {
			
			 WebElement webElement = driver.findElement(By.id(OR.getProperty(key)));
	        	
	         Actions act = new Actions(driver);
	        	
	         act.moveToElement(webElement).perform();;
		}
		log.info("Clicking on an Element : " + key);
		}catch(Throwable t) {
			
			log.info("Error while clicking on an Element : "+key+" error message is: "+t.getMessage());
			Assert.fail(t.getMessage());
		}
	}
	

	
	
	
	
	
	
	

	@BeforeSuite
	public void setUp() {

		if (driver == null) {

			PropertyConfigurator.configure("./src/main/resources/properties/log4j.properties");

			try {
				fis = new FileInputStream("./src/main/resources/properties/Config.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				Config.load(fis);
				log.info("Config properties file loaded");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			try {
				fis = new FileInputStream("./src/main/resources/properties/OR.properties");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				OR.load(fis);
				log.info("OR properties file loaded");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (Config.getProperty("browser").equals("chrome")) {
				
				Map<String, Object> prefs = new HashMap<String, Object>();
				  prefs.put("profile.default_content_setting_values.notifications", 2);
				  prefs.put("credentials_enable_service", false);
				  prefs.put("profile.password_manager_enabled", false);
				 
				 
				 
				  ChromeOptions options = new ChromeOptions();
				  options.setExperimentalOption("prefs", prefs);
				  options.addArguments("--disable-extensions");
				  options.addArguments("--disable-infobars");

				WebDriverManager.chromedriver().setup();
				driver = new ChromeDriver();
				log.info("Chrome browser launched !!!");
			} else if (Config.getProperty("browser").equals("firefox")) {

				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
				log.info("Firefox browser launched !!!");
			}

			driver.get(Config.getProperty("testsiteurl"));
			log.info("Navigated to : " + Config.getProperty("testsiteurl"));
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Integer.parseInt(Config.getProperty("implicit.wait")),
					TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, Integer.parseInt(Config.getProperty("explicit.wait")));
//			try {
//				DbManager.setMysqlDbConnection();
//				log.info("Db Connection established !!!");
//			} catch (ClassNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			
			
			//testEmail.MailSend(args);
			
			//testConfig = new TestConfig();
			
			topNav = new TopNavigation();
			
			/*
			 * try { mail.sendMail(TestConfig.server, TestConfig.from, TestConfig.to,
			 * TestConfig.subject, TestConfig.messageBody, TestConfig.attachmentPath,
			 * TestConfig.attachmentName); } catch (AddressException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); } catch (MessagingException
			 * e) { // TODO Auto-generated catch block e.printStackTrace(); }
			 */

		}

	}
	


//	@AfterSuite
//	public void tearDown() {
//
//		driver.quit();
//		log.info("Test Execution completed !!!");
//	}


}
