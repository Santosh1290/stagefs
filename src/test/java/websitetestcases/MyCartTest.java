package websitetestcases;

import org.testng.annotations.Test;

import utilities.TestUtil;
import webpages.MyCartPage;

public class MyCartTest {
	
	@Test(dataProviderClass=TestUtil.class,dataProvider="getData")
	public void myCartTesting(String FirstName, String LastName, String CompanyName, String StreetName, String HouseNumber, String ZipCode, String City, String PhoneNumber, String Email) {
		
		MyCartPage mycart = new MyCartPage();
		
		mycart.selectPaperType();
		
		mycart.shippingDetails(FirstName, LastName, CompanyName, StreetName, HouseNumber, ZipCode, City, PhoneNumber, Email);
		
		mycart.navToCheckout();
	}

}
