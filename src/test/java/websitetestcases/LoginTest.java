package websitetestcases;

import org.testng.annotations.Test;

import basePage.Page;
import utilities.TestUtil;
import webpages.HomePage;

public class LoginTest extends Page{
	
	@Test(enabled=false, dataProviderClass=TestUtil.class,dataProvider="getData")
	public void LoginTesting(String Email, String Password) {
		
		HomePage login = new HomePage();
		login.doLogin(Email, Password);
		
	}

}
