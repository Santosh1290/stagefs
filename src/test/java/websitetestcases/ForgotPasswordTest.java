package websitetestcases;

import org.testng.annotations.Test;

import basePage.Page;
import utilities.TestUtil;
import webpages.ForgotPassword;

public class ForgotPasswordTest extends Page {
	
	@Test(enabled=false, priority =1)
	public void forgotPassNavTest() {
		
		ForgotPassword forgotNav = new ForgotPassword();
		
		forgotNav.forgotPasswordNav();
		
	}
	
	@Test(enabled=false, priority =2, dataProviderClass=TestUtil.class,dataProvider="getData")
	public void forgotPasswordPopUpTest(String Email) {
		
		ForgotPassword forgotpass = new ForgotPassword();
		
		forgotpass.forgotPasswordPopUp(Email);
	}

}
