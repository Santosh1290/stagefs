package websitetestcases;

import org.testng.annotations.Test;

import webpages.CheckoutPage;

public class CheckOutTest {
	
	@Test
	public void checkoutTesting() {
		
		CheckoutPage checkout = new CheckoutPage();
		
		checkout.buyNow();
		
		checkout.creditCardPayment();
	}

}
