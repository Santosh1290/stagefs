package editorTestCases;

import org.testng.annotations.Test;

import basePage.Page;
import editorPages.PrepareImagesPage;

public class PrepareImagesTest extends Page {
	
	@Test
	public void PrepareImageTesting() {
		
		PrepareImagesPage prepare = new PrepareImagesPage();
		
		prepare.selectAllImages();
		
		prepare.selectCoverImages();
		
		prepare.designOptions();
		
		prepare.autoDesignPopUp();
	}

}
